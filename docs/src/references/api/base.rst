.. _base_class:

Base class
##########

.. automodule:: maicos.core.base
    :members:
    :undoc-members:
    :show-inheritance:
