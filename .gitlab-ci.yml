###########################
######## TEMPLATES ########
###########################

default:
  artifacts:
    expire_in: 1 week
  image:
    name: python:$PYTHON_VERSION

variables:
  PYTHON_VERSION: '3.9'
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip

.python_setup: &python_setup
  - python --version; pip --version
  - python -m pip install -U tox

.linux:
  image: python:$PYTHON_VERSION
  variables:
    OS_NAME: "linux"
  tags:
    - linux
  before_script:
    - *python_setup

.supported_python_versions:
  parallel:
    matrix:
      - PYTHON_VERSION: ['3.9', '3.12']

.windows:
  variables:
    OS_NAME: "windows"
  tags:
    - windows
  before_script:
    - Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
    - choco install -y python --version=$PYTHON_VERSION --no-progress
    - RefreshEnv
    - *python_setup

.macos:
  image: macos-13-xcode-14
  variables:
    OS_NAME: "macos"
  tags:
    - saas-macos-medium-m1
  before_script:
    - export HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1
    - export HOMEBREW_NO_INSTALL_CLEANUP=1
    - brew install --force python@$PYTHON_VERSION
    - export PATH="/usr/local/opt/python@${PYTHON_VERSION}/libexec/bin:$PATH"
    - python$PYTHON_VERSION -m pip install virtualenv || brew install virtualenv
    - python$PYTHON_VERSION -m virtualenv venv || /opt/homebrew/bin/virtualenv venv
    - source venv/bin/activate
    - *python_setup

###########################
######### STAGES ##########
###########################

stages:
  - lint
  - build
  - tests
  - docs
  - wheels
  - deploy

###########################
######### LINT ############
###########################

lint:
  extends: .linux
  stage: lint
  needs: []
  script:
    - python -m tox -e lint

###########################
######### BUILD ###########
###########################

build:linux:
  extends: .linux
  stage: build
  needs: []
  script:
    - python -m tox -e build

build:windows:
  extends: .windows
  stage: build
  needs: []
  script:
    - python -m tox -e build

build:macos:
  extends: .macos
  stage: build
  needs: []
  script:
    - python -m tox -e build

###########################
######### TESTS ###########
###########################

.tests:
  extends: .supported_python_versions
  script:
    - python -m tox -e tests
  stage: tests
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

tests:linux:
  extends:
    - .linux
    - .tests
  needs: ["build:linux"]

tests:windows:
  extends:
    - .windows
    - .tests
  needs: ["build:windows"]

tests:macos:
  extends:
    - .macos
    - .tests
  needs: ["build:macos"]

###########################
######### DOCS ############
###########################

docs:
  extends: .linux
  stage: docs
  needs: []
  script:
    # the following 2 lines are needed to make sure we are on the right branch to check
    # for changes in the CHANGELOG. We omit this on tags, because $CI_COMMIT_BRANCH does
    # not exist and would make git switch fail.
    # ($CI_COMMIT_BRANCH does not exist on tags)
    - "[[ -n $CI_COMMIT_BRANCH ]] && git switch $CI_COMMIT_BRANCH"
    - git fetch origin main
    - python -m tox -e docs
  artifacts:
    paths:
      - dist/docs

###########################
######## DEPLOY ###########
###########################

.build-wheel:
  extends: .supported_python_versions
  stage: wheels
  artifacts:
    paths:
      - dist
  only:
    - tags

build-wheel:linux:
  image: quay.io/pypa/manylinux2014_x86_64
  extends:
    - .build-wheel
  script:
    - /opt/python/cp${PYTHON_VERSION//.}-cp${PYTHON_VERSION//.}/bin/python -m pip install -U tox
    - /opt/python/cp${PYTHON_VERSION//.}-cp${PYTHON_VERSION//.}/bin/python -m tox -e build-linux
    - auditwheel repair dist/*.whl -w dist/
    - rm dist/*linux_x86_64.whl
  needs: ["tests:linux"]

build-wheel:macos:
  extends:
    - .macos
    - .build-wheel
  script:
    - python -m tox -e build
  needs: ["tests:macos"]

build-wheel:windows:
  extends:
    - .windows
    - .build-wheel
  script:
    - python -m tox -e build
  needs: ["tests:windows"]

# The name must be `pages`!
# Otherwise webpage on gitlab.io will not be updated!
pages:
  stage: deploy
  script:
    - cp -r dist/docs public
  only:
    - tags
  artifacts:
    paths:
      - public

pypi:
  stage: deploy
  variables:
    TWINE_USERNAME: $PYPI_USERNAME
    TWINE_PASSWORD: $PYPI_PASSWORD
  script:
    - python -m pip install twine
    - python -m twine upload --verbose --skip-existing dist/*.tar.gz dist/*.whl
  only:
    - tags
