[build-system]
# These are the assumed default build requirements from pip:
# https://pip.pypa.io/en/stable/reference/pip/#pep-517-and-518-support
requires = [
    "cython>=0.28",
    # Numpy requirements taken from
    # https://github.com/MDAnalysis/mdanalysis/blob/develop/package/pyproject.toml
    "numpy==1.22.4; python_version<='3.10' and platform_python_implementation != 'PyPy'",
    "numpy==1.23.2; python_version=='3.11' and platform_python_implementation != 'PyPy'",
    "numpy==1.26.0; python_version=='3.12' and platform_python_implementation != 'PyPy'",
    # For unreleased versions of Python there is currently no known supported
    # NumPy version. In that case we just let it be a bare NumPy install
    "numpy<2.0; python_version>='3.12'",
    "numpy; python_version>='3.9' and platform_python_implementation=='PyPy'",
    "setuptools>=43.0.0",
    "setuptools-git-versioning<2",
    "versioneer[toml]",
    "wheel"]
build-backend = "setuptools.build_meta"

[project]
name = "maicos"
description = "Analyse molecular dynamics simulations of interfacial and confined systems."
authors = [{name = "MAICoS Developer Team"}]
maintainers = [
    {name = "Philip Loche"},
    {name = "Henrik Stooß"},
    {name = "Alexander Schlaich"}
]
readme = "README.rst"
requires-python = ">=3.9"
keywords = ["Science", "Molecular Dynamics", "Confined Systems", "MDAnalysis"]
license = {text = "GPL-3.0-or-later"}
classifiers = [
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Operating System :: POSIX",
    "Operating System :: MacOS :: MacOS X",
    "Operating System :: Microsoft :: Windows",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Programming Language :: C",
    "Topic :: Scientific/Engineering",
    "Topic :: Scientific/Engineering :: Bio-Informatics",
    "Topic :: Scientific/Engineering :: Chemistry",
    "Topic :: Scientific/Engineering :: Physics",
    "Topic :: Software Development :: Libraries :: Python Modules",
    "Topic :: System :: Shells",
]
dependencies = [
    "mdacli>=0.1.28",
    "MDAnalysis>=2.7.0",
    "numpy>=1.22.3",
    "scipy>=1.0.0",
    "tqdm>=4.60.0",
    "typing_extensions",
]
dynamic = ["version"]

[project.urls]
homepage = "https://www.maicos-analysis.org"
documentation = "https://maicos-devel.gitlab.io/maicos"
repository = "https://gitlab.com/maicos-devel/maicos"
changelog = "https://maicos-devel.gitlab.io/maicos/get-started/changelog.html"
issues = "https://gitlab.com/maicos-devel/maicos/-/issues"
discord = "https://discord.gg/mnrEQWVAed"
twitter = "https://twitter.com/maicos_analysis"

[project.optional-dependencies]
examples = [
    "matplotlib",
]

[project.scripts]
maicos = "maicos.__main__:main"

[tool.setuptools.packages.find]
where = ["src"]

# configuration for the isort module
[tool.isort]
profile = "black"
line_length = 88
indent = 4
include_trailing_comma = true
lines_after_imports = 2
known_first_party = "maicos"
skip = "src/maicos/_version.py"

[tool.black]
exclude = "src/maicos/_version.py"

[tool.pytest.ini_options]
testpaths = "tests"

[tool.check-manifest]
# ignore files missing in VCS
ignore = ["src/maicos/lib/_cmath.c"]

[tool.versioneer]
VCS = "git"
style = "pep440"
versionfile_source = "src/maicos/_version.py"
versionfile_build = "maicos/_version.py"
tag_prefix = "v"

[tool.mypy]
ignore_missing_imports = true

[tool.coverage.paths]
source = [
    "src/maicos",
    "*/site-packages",
]

[tool.coverage.report]
skip_covered = true
show_missing = true
exclude_lines = [
    "if __name__ == .__main__.:",
]
